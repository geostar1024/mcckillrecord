package net.minecraftcenter.MCCKillRecord;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;


public class MCCKillRecordListener implements Listener {

    /**
     * Reference to main plugin
     */
    private MCCKillRecord plugin;

    public MCCKillRecordListener(MCCKillRecord plugin) {
        this.plugin = plugin;
    }
    

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onEntityDamage(PlayerDeathEvent event) {
    	
    	// get the time of death (as of the triggering of this event) and convert it to a date string
    	long deathTime = System.currentTimeMillis();
    	
    	// get the name of the cause of death
    	String damageCause=event.getEntity().getLastDamageCause().getCause().name();
   
    	String playerKiller="nonplayer";
    	// if the killer is null, the kill was not done by a player
    	if (event.getEntity().getKiller()!=null){
    		playerKiller=event.getEntity().getKiller().getName();
    	}
    	
    	// takes in the time of death in milliseconds, the player that was killed, the killer, the final cause of death, and the death message
    	int status = plugin.writeKillRecord(deathTime,event.getEntity().getName(),playerKiller,damageCause,event.getDeathMessage());

    	if (status==0){
    		System.out.println("death recorded!");
    	}
    	else {
    			System.out.println("[ERROR] death recording failed! you should check on this!");
    		}
    }
}
